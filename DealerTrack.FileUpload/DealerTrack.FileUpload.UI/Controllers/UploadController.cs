﻿using DealerTrack.FileUpload.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DealerTrack.FileUpload.UI.Helpers;
using System.IO;
using System.Collections.Specialized;
using System.Web.Hosting;
using Microsoft.VisualBasic.FileIO;
using DealerTrack.FileUpload.UI.Models;

namespace DealerTrack.FileUpload.UI.Controllers
{
    [RoutePrefix("api/v1/Upload")]
    public class UploadController : ApiController
    {
        #region Members
        private readonly IDealsCsvParser _parser = null;
        #endregion

        #region Constructors
        public UploadController(IDealsCsvParser parser)
        {
            _parser = parser;
        }
        #endregion

        #region Methods
        [HttpPost]
        [Route("")]
        public async Task<HttpResponseMessage> PostFormData()
        {
            Logger.Debug("Post Form Data Action is starting");

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                Logger.Debug("Unsupported Media Type");
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.UnsupportedMediaType));
            }

            var deals = new List<Deal>();

            var path = Path.GetTempPath();
            Logger.DebugFormat("Temp path is ({0})", path);

            MultipartFormDataStreamProvider streamProvider = new MultipartFormDataStreamProvider(path);

            await Request.Content.ReadAsMultipartAsync(streamProvider);

            if (streamProvider.FileData != null)
            {
                #region Save File
                var fileData = streamProvider.FileData[0];

                string fileName = "";

                if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                {
                    Logger.Debug("No file name associated with the uploaded file. Generating a new name");
                    fileName = Guid.NewGuid().ToString();
                }
                else
                {
                    fileName = fileData.Headers.ContentDisposition.FileName;
                }

                if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                {
                    fileName = fileName.Trim('"');
                }
                if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                {
                    fileName = Path.GetFileName(fileName);
                }

                Logger.DebugFormat("File name is ({0})", fileName);

                var filePath = Path.Combine(HostingEnvironment.MapPath("~/App_Data"), fileName);

                Logger.DebugFormat("File path is ({0})", filePath);

                var fileInfo = new FileInfo(filePath);
                if (fileInfo.Exists)
                {
                    Logger.DebugFormat("The file ({0}) already exists. Deleting it", filePath);
                    File.Delete(filePath);
                }

                File.Move(fileData.LocalFileName, filePath);
                Logger.DebugFormat("File ({0}) has been saved successfully", filePath);
                #endregion

                #region Parsing
                deals = _parser.Parse(filePath);
                #endregion
            }

            Logger.Debug("Post Form Data Action has ended");

            var response = Request.CreateResponse<List<Deal>>(HttpStatusCode.OK, deals);
            return response;
        }
        #endregion
    }
}
