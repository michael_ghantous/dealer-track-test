﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DealerTrack.FileUpload.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerTrack.FileUpload.UI.DI
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IDealsCsvParser>().ImplementedBy<DealsCsvParser>().LifestylePerWebRequest());
        }
    }
}