﻿using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using DealerTrack.FileUpload.UI.DI;
using DealerTrack.FileUpload.UI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace DealerTrack.FileUpload.UI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        #region Members
        private static IWindsorContainer _container;
        #endregion

        #region Methods
        protected void Application_Start()
        {
            Logger.Debug("Application is starting");

            Logger.Debug("Configuring Windsor");
            ConfigureWindsor(GlobalConfiguration.Configuration);
            Logger.Debug("Windsor configured successfully");

            Logger.Debug("Configuring Web API");
            GlobalConfiguration.Configure(c => WebApiConfig.Register(c, _container));
            Logger.Debug("Web API configured successfully");
        }

        private static void ConfigureWindsor(HttpConfiguration configuration)
        {
            _container = new WindsorContainer();

            _container.Install(FromAssembly.This());
            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel, true));

            var dependencyResolver = new WindsorDependencyResolver(_container);
            configuration.DependencyResolver = dependencyResolver;
        }

        protected void Application_End()
        {
            Logger.Debug("Application is ending");

            _container.Dispose();
            base.Dispose();

            Logger.Debug("Application ended successfully");
        }
        #endregion
    }
}
