﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DealerTrack.FileUpload.UI.Models;
using Microsoft.VisualBasic.FileIO;

namespace DealerTrack.FileUpload.UI.Services
{
    public class DealsCsvParser : IDealsCsvParser
    {
        #region Methods
        public List<Deal> Parse(string filePath)
        {
            var deals = new List<Deal>();

            using (TextFieldParser parser = new TextFieldParser(filePath))
            {
                parser.SetDelimiters(new string[] { "," });
                parser.HasFieldsEnclosedInQuotes = true;

                parser.ReadLine();

                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();

                    var deal = new Deal();
                    deal.Number = fields[0].ToStringSafe(); 
                    deal.CustomerName = fields[1].ToStringSafe();
                    deal.DealershipName = fields[2].ToStringSafe();
                    deal.Vehicle = fields[3].ToStringSafe();
                    deal.Price = fields[4].ToDoubleOrDefault();
                    deal.Date = fields[5].ToDateTimeOrDefault();

                    deals.Add(deal);
                }
            }

            return deals;
        }
        #endregion
    }
}