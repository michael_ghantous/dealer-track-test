﻿using DealerTrack.FileUpload.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerTrack.FileUpload.UI.Services
{
    public interface IDealsCsvParser
    {
        #region Methods
        List<Deal> Parse(string filePath);
        #endregion
    }
}