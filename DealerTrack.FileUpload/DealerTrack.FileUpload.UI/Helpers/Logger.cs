﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealerTrack.FileUpload.UI.Helpers
{
    public class Logger
    {
        #region Members
        private static ILog _logManager = null;
        #endregion

        #region Properties
        private static ILog LogManager
        {
            get
            {
                if (_logManager == null)
                {
                    _logManager = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    log4net.Config.XmlConfigurator.Configure();
                }

                return _logManager;
            }
        }
        #endregion

        #region Methods
        public static void Debug(string message)
        {
            LogManager.Debug(message);
        }

        public static void Debug(string message, Exception ex)
        {
            LogManager.Debug(message, ex);
        }

        public static void DebugFormat(string format, params object[] args)
        {
            LogManager.DebugFormat(format, args);
        }

        public static void Info(string message)
        {
            LogManager.Info(message);
        }

        public static void Info(string message, Exception ex)
        {
            LogManager.Info(message, ex);
        }

        public static void InfoFormat(string format, params object[] args)
        {
            LogManager.InfoFormat(format, args);
        }

        public static void Error(string message)
        {
            LogManager.Error(message);
        }

        public static void Error(string message, Exception ex)
        {
            LogManager.Error(message, ex);
        }

        public static void ErrorFormat(string format, params object[] args)
        {
            LogManager.ErrorFormat(format, args);
        }

        public static void Fatal(string message)
        {
            LogManager.Fatal(message);
        }

        public static void Fatal(string message, Exception ex)
        {
            LogManager.Fatal(message, ex);
        }

        public static void FatalFormat(string format, params object[] args)
        {
            LogManager.FatalFormat(format, args);
        }
        #endregion
    }
}