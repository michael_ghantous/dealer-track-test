﻿using Castle.Windsor;
using DealerTrack.FileUpload.UI.DI;
using DealerTrack.FileUpload.UI.Filers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace DealerTrack.FileUpload.UI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config, IWindsorContainer container)
        {
            RemoveXmlFormatter(config);
            ConfigureJsonFormatter(config);
            RegisterFilters(config);
            MapRoutes(config);
            RegisterControllerActivator(container);
        }

        private static void RemoveXmlFormatter(HttpConfiguration config)
        {
            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);
        }

        private static void ConfigureJsonFormatter(HttpConfiguration config)
        {
            var formatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            formatter.SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.Objects,
                DateFormatString = "yyyy MMM dd"
            };
        }

        private static void MapRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void RegisterControllerActivator(IWindsorContainer container)
        {
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator),
                new WindsorCompositionRoot(container));
        }

        private static void RegisterFilters(HttpConfiguration config)
        {
            config.Filters.Add(new CustomExceptionFilter());
        }
    }
}
